<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211208155215 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA recursos_humanos');
        $this->addSql('CREATE SEQUENCE recursos_humanos.empleado_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE recursos_humanos.empleado (id INT NOT NULL, nombre VARCHAR(120) NOT NULL, area INT NOT NULL, fecha_nacimiento DATE DEFAULT NULL, activo BOOLEAN NOT NULL, fecha_creacion TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, fecha_actualizacion TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, usuario_actualizo INT NOT NULL, numero_empleado SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9EC9B2CF78CE7832 ON recursos_humanos.empleado (numero_empleado)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE recursos_humanos.empleado_id_seq CASCADE');
        $this->addSql('DROP TABLE recursos_humanos.empleado');
    }
}
