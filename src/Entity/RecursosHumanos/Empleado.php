<?php

namespace App\Entity\RecursosHumanos;

use App\Repository\RecursosHumanos\EmpleadoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmpleadoRepository::class)
 * @ORM\Table(name="recursos_humanos.empleado")
 */
class Empleado
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $nombre;

    /**
     * @ORM\Column(type="integer")
     */
    private $area;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fechaNacimiento;

    /**
     * @ORM\Column(type="boolean")
     */
    private $activo;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fechaCreacion;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fechaActualizacion;

    /**
     * @ORM\Column(type="integer")
     */
    private $usuarioActualizo;

    /**
     * @ORM\Column(type="smallint", unique=true)
     */
    private $numeroEmpleado;

    public function __construct()
    {
        $this->fechaActualizacion = new \DateTime();
        $this->fechaCreacion      = new \DateTime();
    }
    //Métodos generados autómaticamente

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getArea(): ?int
    {
        return $this->area;
    }

    public function setArea(int $area): self
    {
        $this->area = $area;

        return $this;
    }

    public function getFechaNacimiento(): ?\DateTimeInterface
    {
        return $this->fechaNacimiento;
    }

    public function setFechaNacimiento(?\DateTimeInterface $fechaNacimiento): self
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }

    public function getActivo(): ?bool
    {
        return $this->activo;
    }

    public function setActivo(bool $activo): self
    {
        $this->activo = $activo;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(?\DateTimeInterface $fechaActualizacion): self
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    public function getUsuarioActualizo(): ?int
    {
        return $this->usuarioActualizo;
    }

    public function setUsuarioActualizo(int $usuarioActualizo): self
    {
        $this->usuarioActualizo = $usuarioActualizo;

        return $this;
    }

    public function getNumeroEmpleado(): ?int
    {
        return $this->numeroEmpleado;
    }

    public function setNumeroEmpleado(int $numeroEmpleado): self
    {
        $this->numeroEmpleado = $numeroEmpleado;

        return $this;
    }
}
