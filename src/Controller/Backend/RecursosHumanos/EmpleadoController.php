<?php

namespace App\Controller\Backend\RecursosHumanos;

use App\Entity\RecursosHumanos\Empleado;
use App\Form\RecursosHumanos\EmpleadoType;
use App\Repository\RecursosHumanos\EmpleadoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/recursos_humanos/empleado")
 */
class EmpleadoController extends AbstractController
{
    /**
     * @Route("/", name="backend_recursos_humanos_empleado_index", methods={"GET"})
     */
    public function index(EmpleadoRepository $empleadoRepository): Response
    {
        return $this->render('backend/recursos_humanos/empleado/index.html.twig', [
            'empleados' => $empleadoRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="backend_recursos_humanos_empleado_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $empleado = new Empleado();
        $form = $this->createForm(EmpleadoType::class, $empleado);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //Verificar si ya existe el número de empleado antes de crear
            //Si ya existe redireccionar al index
            $empleado->setUsuarioActualizo(1);
            $entityManager->persist($empleado);
            $entityManager->flush();

            return $this->redirectToRoute('backend_recursos_humanos_empleado_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backend/recursos_humanos/empleado/new.html.twig', [
            'empleado' => $empleado,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="backend_recursos_humanos_empleado_show", methods={"GET"})
     */
    public function show(Empleado $empleado): Response
    {
        return $this->render('backend/recursos_humanos/empleado/show.html.twig', [
            'empleado' => $empleado,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="backend_recursos_humanos_empleado_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Empleado $empleado, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(EmpleadoType::class, $empleado);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $empleado->setUsuarioActualizo(2);
            $entityManager->flush();

            return $this->redirectToRoute('backend_recursos_humanos_empleado_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backend/recursos_humanos/empleado/edit.html.twig', [
            'empleado' => $empleado,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="backend_recursos_humanos_empleado_delete", methods={"POST"})
     */
    public function delete(Request $request, Empleado $empleado, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$empleado->getId(), $request->request->get('_token'))) {
            $entityManager->remove($empleado);
            $entityManager->flush();
        }

        return $this->redirectToRoute('backend_recursos_humanos_empleado_index', [], Response::HTTP_SEE_OTHER);
    }
}
